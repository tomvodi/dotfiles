# How to setup nvim

Configuration based on [this site](https://dougblack.io/words/a-good-vimrc.html).
Uses [vim-plug](https://github.com/junegunn/vim-plug) as plugin manager.