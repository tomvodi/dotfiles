" Plugins with vim-plug {{{

call plug#begin('~/.vim/plugged')

Plug 'dracula/vim',{'name':'dracula'}
Plug 'sjl/gundo.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jeetsukumaran/vim-buffergator'
Plug 'tpope/vim-fugitive'
Plug 'fatih/vim-go'

call plug#end()

" }}}

" Colors {{{

colorscheme dracula

syntax enable

" }}}

" Spaces & Tabs {{{

set tabstop=2

set softtabstop=2
set shiftwidth=2

set expandtab

set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
set list

" }}}

" Leader Shortcuts {{{

let mapleader=","   " leader is comma

" jk is escape
inoremap jk <esc>

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>

" Session handling

nnoremap <leader>s :mks! ~/.config/nvim/default-session.vim<CR>
nnoremap <leader>a :source ~/.config/nvim/default-session.vim<CR>

" }}}

" UI config {{{

set number

set showcmd

set cursorline

filetype indent on

set smartindent

set wildmenu

set lazyredraw

set showmatch

" }}}

" FZF Mappings {{{

nnoremap <C-p> :<C-u>FZF<CR>
nnoremap <C-ü> :<C-u>Buffers<CR>

let $FZF_DEFAULT_COMMAND = 'rg --files'


" }}}

" netrw {{{

let g:netrw_banner = 0
nnoremap <leader>x :Explore<CR>

" }}}

" Windows {{{

set splitbelow 
set splitright

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <silent> <leader>d :bp\|bd #<CR>
nnoremap <leader>ö :12split \| :set wfh \| :terminal<CR>

augroup VCenterCursor
  au!
  au BufEnter,WinEnter,WinNew,VimResized *,*.*
     \ let &scrolloff=winheight(win_getid())/2
augroup END

" }}}

" Searching {{{

set incsearch 
set hlsearch

nnoremap <leader><space> :nohlsearch<CR>

" }}}

" Folding {{{

set foldenable
set foldlevelstart=10
set foldnestmax=10

nnoremap <space> za

set foldmethod=indent

" }}}

" Movement {{{

" move to beginning/end of line
nnoremap B ^
nnoremap E $

" }}}

" Status line {{{

set noshowmode

" }}}

" Terminal mode {{{

tnoremap <C-ö> <C-\><C-N>

" }}}

" vim go {{{

let g:go_def_mapping_enabled = 0

" }}}

" coc {{{

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use U to show documentation in preview window
nnoremap <silent> U :call <SID>show_documentation()<CR>

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" }}}

" Bufferogator {{{

let g:buffergator_suppress_keymaps=1
let g:buffergator_viewport_split_policy="b"
let g:buffergator_show_full_directory_path=0
let g:buffergator_split_size=8
let g:buffergator_sort_regime="mru"
nnoremap <leader>b :BuffergatorToggle<CR>

" }}}


set modelines=1

" vim:foldmethod=marker:foldlevel=0
